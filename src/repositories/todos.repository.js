const Todos = require('../models/todos.model');

module.exports = {
    getTodos: async () => {
        const todos = await Todos.find({});
        return todos;
    },

    createTodo: async (request) => {
        const todo = new Todos(request);
        const createdTodo = await todo.save();
        return createdTodo;
    },

    getTodo: async (id) => {
        const todo = await Todos.findById(id);
        return todo;
    },

    deleteTodo: async (todo) => {
        await todo.remove();
    }
};
