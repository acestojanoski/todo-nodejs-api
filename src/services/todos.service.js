const todosRepository = require('../repositories/todos.repository');

module.exports = {
    getTodos: async () => {
        const todos = await todosRepository.getTodos();
        return todos;
    },

    createTodo: async (req) => {
        const todo = await todosRepository.createTodo(req.body);
        return todo;
    },

    getTodo: async (req) => {
        const todo = await todosRepository.getTodo(req.params.id);
        return todo;
    },

    deleteTodo: async (req) => {
        const todo = await todosRepository.getTodo(req.params.id);

        if (!todo) {
            return null;
        }

        await todosRepository.deleteTodo(todo);
        return todo;
    }
};
