const express = require('express');
const todosController = require('../controllers/todos.controller');

const router = express.Router();

router.route('/todos')
    .get(todosController.getTodos)
    .post(todosController.createTodo);

router.route('/todos/:id')
    .get(todosController.getTodo)
    .delete(todosController.deleteTodo);

module.exports = router;
