const todosService = require('../services/todos.service');

module.exports = {
    getTodos: async (req, res, next) => {
        try {
            const todos = await todosService.getTodos();
            res.status(200).json(todos);
        } catch (err) {
            next(err);
        }
    },

    createTodo: async (req, res, next) => {
        try {
            const todo = await todosService.createTodo(req);
            res.status(201).json(todo);
        } catch (err) {
            next(err);
        }
    },

    getTodo: async (req, res, next) => {
        try {
            const todo = await todosService.getTodo(req);
            if (!todo) {
                return res.status(404).json({
                    error: 'Not found.',
                })
            }
            res.status(200).json(todo);
        } catch (err) {
            next(err);
        }
    },

    deleteTodo: async (req, res, next) => {
        try {
            const todo = await todosService.deleteTodo(req);
            if (!todo) {
                return res.status(404).json({
                    error: 'Not found.',
                })
            }
            res.status(200).json(todo);
        } catch (err) {
            next(err);
        }
    }
};
