const cors = require('cors');
const chalk = require('chalk');
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const todosRoute = require('./routes/todos.route');

require('dotenv').config();

const app = express();

app.use(cors());
app.use(bodyParser.urlencoded({
    extended: true,
}));
app.use(bodyParser.json());

mongoose.Promise = global.Promise;
mongoose.connect(process.env.CONNECTION_STRING, { useNewUrlParser: true }, (err) => {
    if (err) {
        console.error(chalk.red('Failed to connect with the database.'));
    } else {
        console.log(chalk.blue('Connected to the database.'));
    }
});

app.use('/api', todosRoute);

app.use(
    (err, req, res, next) => {
        if (err.kind === 'ObjectId') {
            return res.status(404).json({
                error: 'Not found',
            });
        }

        res.status(500).json({
            error: err.message,
        });
    }
);

app.listen('5000', () => {
    console.log(chalk.yellow('App is running on port 5000.'));
})
