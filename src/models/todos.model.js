const mongoose = require('mongoose');

const Schema = mongoose.Schema;
const modelName = 'todos';

const todosSchema = new Schema({
    description: {
        type: String,
        required: true,
    }
}, { collection: modelName });

module.exports = mongoose.model(modelName, todosSchema);
