# Todo API

## Description

Simple Todo API created with Node.js

## Technologies and libraries

*   [Node.js](https://nodejs.org/en/)
*   [express](https://expressjs.com/)
*   [mongoose](https://mongoosejs.com/)
*   [cors](https://github.com/expressjs/cors)
*   [body-parser](https://github.com/expressjs/body-parser)
*   [dotenv](https://github.com/motdotla/dotenv)
*   [nodemon](https://nodemon.io/)
*   [chalk](https://github.com/chalk/chalk)

## Running

Execute `npm install` at the root level.
Create .env file at the root level, and add your connection string.

Example:

CONNECTION_STRING=Your_connection_string_here

Execute `npm run start` to start it.

Execute `npm run dev` to start it with nodemon, so you can make changes while running.
